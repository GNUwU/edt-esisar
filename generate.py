import configparser
import urllib3
import datetime
import re

def generateEDT(id):
    year = datetime.datetime.now().year
    http = urllib3.PoolManager()
    headers = urllib3.util.make_headers(basic_auth=config.get('AGALAN', 'AGALAN_LOGIN') + ":" + config.get('AGALAN', 'AGALAN_PASSWORD'))
    r = http.request(
        'GET',
        'https://edt.grenoble-inp.fr/directCal/2020-2021/etudiant/esisar?resources=' + id.rstrip() + '&startDay=31&startMonth=08&startYear=' + str(int(year)-2) + '&endDay=10&endMonth=01&endYear=' + str(int(year)+3),
        headers=headers)

    result = r.data.decode('utf-8').splitlines()
    formatDescription(result)
    return result

def formatDescription(result): # Pass by reference, don't forget
    for idx, line in enumerate(result):
        if line.startswith('SUMMARY'):
            if "HA_" in line:
                result[idx] = result[idx].replace('_', ' ')

        elif line.startswith('LOCATION'):
            result[idx] = result[idx].replace(" (V)", "")
            if "A166_CM" in line:
                result[idx] = "LOCATION:A166"

        elif line.startswith('DESCRIPTION'):
            while(not result[idx+1].startswith('UID')): # Some descriptions are formed on multiple lines, this will concatenate
                line = line + result[idx+1][1:]
                del result[idx+1]

            parts = line.split('\\n') # 0: DESC, 1: <empty>, 2: course, 3: teacher, 4: export data, 5: <empty>
            course = parts[2]
            realcourse = result[idx-2].split(':')[1]

            if course[0].isdigit(): # Remove some malformed entry, harder to parse
                courseparts = course.split('_') # 0: <promo>M<course>, 1: year, 2: semester, 3: type, 4: group (most of the time)
                if course.startswith("1A") and not re.search(r'1AM[A-Z]{2}\d{3}_\d{4}_S\d_[A-Z]{2}_[A-Z]\d', course): # This will check if this is default case or not
                    if "HA" in result[idx-2]: # Check if SUMMARY is "HA"
                        if parts[3][0].isdigit():
                            result[idx] = "DESCRIPTION:Kholle avec " + parts[4] + ', de ' + parts[3]
                        elif parts[3].startswith('('):
                            result[idx] = "DESCRIPTION:Kholle avec eleves"
                        else:
                            result[idx] = "DESCRIPTION:Kholle avec " + parts[3]
                    else:
                        result[idx] = "DESCRIPTION:" + course
                elif course.startswith("2A") and not re.search(r'2AM[A-Z]{2}\d{3}_\d{4}_S\d_[A-Z]{2}_[A-Z]\d', course):
                    result[idx] = "DESCRIPTION:" + course
                elif course.startswith("3A") and re.search(r'3AM[A-Z]{2}\d{3}_\d{4}_S\d_PROJET_[A-Z]\d', course): # Match only PROJET type, because there is two groups (print it, you'll see what I mean)
                    result[idx] = "DESCRIPTION:" + realcourse + " en " + courseparts[3] + " avec " + parts[4]
                elif re.match(r'^\dA(PP)?((-S\d(-TP\d[A-C])?)|-MISTRE)?$', course): # Matches "3A", "3A-S4", "3A-S4-TP1A", "5A-MISTRE", "4APP" for example, because they are common classes
                    result[idx] = "DESCRIPTION:" + ", ".join(parts[2:-2])
                elif re.match(r'^3AM[A-Z]{2}\d{3}_\d{4}_S\d_IUT_[A-Z]{2}_[A-Z]\d$', course):
                    result[idx] = "DESCRIPTION:Cours de soutien en " + realcourse + " avec " + ", ".join(parts[3:-2])
                elif re.match(r'^3AM[A-Z]{2}\d{3}_\d{4}_S\d_[A-Z]{2}_CPGE_[A-Z]\d$', course): #Nearly the same as higher but for CPGE. "CPGE" and "TP" are swapped, so can't match it correctly with one regex
                    result[idx] = "DESCRIPTION:Cours de soutien en " + realcourse + " avec " + ", ".join(parts[3:-2])
                # Add cases here
                else: # Default case
                    if len(parts) >= 6 and len(courseparts) == 5: # One more security to avoid IndexOutOfRange
                        if courseparts[3] == "TP" or courseparts[3] == "TD": 
                            result[idx] = "DESCRIPTION:" + realcourse + " en " + courseparts[3] + courseparts[4][1:] + " avec " + ", ".join(parts[3:-2])
                        else:
                            result[idx] = "DESCRIPTION:" + realcourse + " en " + courseparts[3] + " avec " + ", ".join(parts[3:-2])
                    else:
                        result[idx] = line

            elif re.search(r'AM[A-Z]{2}\d{3}_\d{4}_S\d_[A-Z ]+_[A-Z]\d', parts[3]): # Merci Toupance d'avoir inverse le pattern
                result[idx] = "DESCRIPTION:" + realcourse + " en " + parts[3].split('_')[3] + " avec " + parts[2]
            else:
                result[idx] = line

config = configparser.ConfigParser()
config.read('config.ini')

# opening ID file
ids = open('IDS.txt', 'r')

for id in ids:
    data = generateEDT((id.split(';')[1]))
    f = open(config.get("Config", "OutputDIR") + id.split(';')[0] + ".ics", 'w')
    f.write("\n".join(data))
    f.close()
    print("Generated " + id.split(';')[0] + ".ics !")

print("Finished.")
