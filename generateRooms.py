import configparser
import urllib3
import datetime
import re
from datetime import datetime
def generateEDT(id):
    year = datetime.now().year

    http = urllib3.PoolManager()
    headers = urllib3.util.make_headers(basic_auth=config.get('AGALAN', 'AGALAN_LOGIN') + ":" + config.get('AGALAN', 'AGALAN_PASSWORD'))
    r = http.request(
        'GET',
        'https://edt.grenoble-inp.fr/directCal/2020-2021/etudiant/esisar?resources=' + str(id) + '&startDay=31&startMonth=08&startYear=' + str(int(year)-2) + '&endDay=10&endMonth=01&endYear=' + str(int(year)+3),
        headers=headers)

    result = r.data.decode('utf-8').splitlines()
    return result

config = configparser.ConfigParser()
config.read('config.ini')

# opening ID file
ids = open('Rooms-IDS.txt', 'r')

for id in ids:
    data = generateEDT(int(id.split(',')[1]))
    f = open(config.get("Config", "RoomsOutputDir") + id.split(',')[0] + ".ics", 'w')
    f.write("\n".join(data))
    f.close()
    print("Generated " + id.split(',')[0] + ".ics !")

print("Finished.")
