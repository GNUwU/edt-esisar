<?php
// Formatted with "12:34"
function generateDateTime(DateTime $dt, string $value) : DateTime {
    $dt->setTimeZone(new DateTimeZone('Europe/Paris')); // Set timezone because fuck
    $startHour = intval(explode(':', $value)[0]);
    $startMin = intval(explode(':', $value)[1]);
    $dt->setTime($startHour, $startMin);

    return $dt;
}

function datesFromIndex(string $value, string $date): array {
    $start = new DateTime();
    $startTime = explode('-', $value)[0]; // $value format is hh:mm-hh:mm
    $start = generateDateTime($start, $startTime);

    $end = new DateTime();
    $endTime = explode('-', $value)[1];
    $end = generateDateTime($end, $endTime);

    if ($date !== '') {
        $start->setDate(explode('-', $date)[0], explode('-', $date)[1], explode('-', $date)[2]);
        $end->setDate(explode('-', $date)[0], explode('-', $date)[1], explode('-', $date)[2]);
    }

    return [$start, $end];
}

function areDatesCrossing(DateTime $start1, DateTime $end1, DateTime $start2, DateTime $end2): bool {
    return $start1 <= $end2 && $end1 >= $start2;
}

$available = [];
$notavailable = [];
$formaterror = false;

if (isset($_POST['slot']) && (($_POST['slot'] != '' && $_POST['slot'] != 'custom') || ($_POST['slot'] == 'custom' && $_POST['custom-input'] != ''))) {
    if ($_POST['slot'] != 'custom') {
        $selected = datesFromIndex($_POST['slot'], $_POST['date']); // Get array of DateTime objects representing the start and the end of time slot
    } else {
        if (preg_match('/^([01]\d|2[0-3]):[0-5]\d-([01]\d|2[0-3]):[0-5]\d$/', $_POST['custom-input'])) {
            $selected = datesFromIndex($_POST['custom-input'], $_POST['date']);
        } else {
            $formaterror = true;
        }
    }

    if (!$formaterror) {
        $files = new FilesystemIterator('../rooms');

        foreach ($files as $file) { // Cycle through ICal file of each room
            $lines = file('../rooms/'.$file->getFilename());
            $found = false;

            $idx = 5;
            while ($idx < sizeof($lines) && !$found) {
                if (str_contains($lines[$idx], 'DTSTART') !== false) {
                    $start1 = new DateTime(explode(':', $lines[$idx])[1]);
                    $end1 = new DateTime(explode(':', $lines[$idx+1])[1]);

                    $idx += 10; // Skipping lines

                    if (areDatesCrossing($start1, $end1, $selected[0], $selected[1])) {
                        $found = true;
                    }
                }

                $idx++;
            }

            if ($found) {
                array_push($notavailable, $file->getBasename('.ics'));
            } else {
                array_push($available, $file->getBasename('.ics')); // Push name of room in array, will be printed later
            }
        }

        sort($available); // Sort arrays by name, to find quickly what we're searching. Idk why it isn't sorted by FilesystemIterator
        sort($notavailable);
    }
} else {
    $_POST['slot'] = '';
    $date = new DateTime();
    $_POST['date'] = $date->format('Y-m-d');
    $_POST['custom-input'] = '';
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EDT Esisar</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/data/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/data/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/data/favicon-16x16.png">
    <link rel="manifest" href="/data/site.webmanifest">
    <link rel="mask-icon" href="/data/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.3/build/pure-min.css" crossorigin="anonymous">
    <style>
        .container {
            display: flex;
            justify-content: center;
            flex-direction: column;
            align-items: center;
        }

        ul {
            padding-left: 20px;
        }

        .error {
            margin-top: 15px;
            color: red;
            font-weight: bold;
        }

        @media screen and (max-width: 680px) {
            .flex-form {
                display: flex;
                justify-content: center;
                flex-direction: column;
                align-items: center;
            }

            .flex-form > *:not(:last-child) {
                margin-bottom: 7px !important;
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Trouver une salle</h1>
        <a href="/output">C'est ici les calendriers .ics</a>
        <?php if ($formaterror): ?>
        <span class="error">Erreur de format !</span>
        <?php endif; ?>
        <form action="" method="POST" class="pure-form flex-form" style="margin-top: 15px;">
            <input type="date" name="date" value="<?= $_POST['date']; ?>">
            <select name="slot" id="slot">
                <option value="">--Choisissez le créneau--</option>
                <option value="08:15-09:45" <?= $_POST['slot'] == '08:15-09:45' ? 'selected' : ''; ?>>8h15-9h45</option>
                <option value="10:00-11:30" <?= $_POST['slot'] == '10:00-11:30' ? 'selected' : ''; ?>>10h-11h30</option>
                <option value="13:00-14:30" <?= $_POST['slot'] == '13:00-14:30' ? 'selected' : ''; ?>>13h-14h30</option>
                <option value="14:45-16:15" <?= $_POST['slot'] == '14:45-16:15' ? 'selected' : ''; ?>>14h45-16h15</option>
                <option value="16:30-18:00" <?= $_POST['slot'] == '16:30-18:00' ? 'selected' : ''; ?>>16h30-18h</option>
                <option value="18:15-19:45" <?= $_POST['slot'] == '18:15-19:45' ? 'selected' : ''; ?>>18h15-19h45</option>
                <option id="custom-option" value="custom" <?= $_POST['slot'] == 'custom' ? 'selected' : ''; ?>>Custom...</option>
            </select>
            <input id="custom-input" name="custom-input" type="text" placeholder="Format : '08:00-14:23'" value="<?= $_POST['custom-input']; ?>" style="<?= $_POST['slot'] == 'custom' ? 'display: inline-block;' : 'display: none;';?>"/>
            <input type="submit" value="Vérifier" class="pure-button pure-button-primary"/>
        </form>
        <ul style="margin-bottom: 0px;">
            <?php foreach ($available as $room): ?>
                <li><strong><?= $room; ?></strong> est libre pour ce créneau !</li>
            <?php endforeach; ?>
        </ul>
        <ul>
            <?php foreach ($notavailable as $room): ?>
                <li style="color: #8f8f8f"><strong><?= $room; ?></strong> n'est pas libre pour ce créneau...</li>
            <?php endforeach; ?>
        </ul>
    </div>
</body>
<script>
let opt = document.querySelector("#slot");
opt.addEventListener('change', (e) => {
    if (opt.options[opt.options.selectedIndex].value == 'custom')
        document.querySelector('#custom-input').style.display = 'inline-block';
    else
        document.querySelector('#custom-input').style.display = 'none';
});
</script>
</html>